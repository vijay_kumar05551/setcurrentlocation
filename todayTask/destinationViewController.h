//
//  destinationViewController.h
//  todayTask
//
//  Created by Click Labs130 on 11/4/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface destinationViewController : UIViewController
@property(nonatomic,strong)NSMutableArray* destData;
@property(nonatomic,strong)NSString* myCity;
@property(nonatomic,strong)NSString* myState;
@property(nonatomic,strong)NSString* MyCountry;
@property(nonatomic,strong)NSString* myFormattedAddress;



@end
