//
//  destinationViewController.m
//  todayTask
//
//  Created by Click Labs130 on 11/4/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "destinationViewController.h"

@interface destinationViewController ()
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) IBOutlet UITextField *stateTextField;
@property (strong, nonatomic) IBOutlet UILabel *cityLabel;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UILabel *addrssLabel;
@property (strong, nonatomic) IBOutlet UILabel *addLabel;
@property (strong, nonatomic) IBOutlet UILabel *countyLabel;

@end

@implementation destinationViewController
@synthesize nameLabel;
@synthesize nameTextField;
@synthesize stateLabel;
@synthesize stateTextField;
@synthesize cityLabel;
@synthesize cityTextField;
@synthesize addrssLabel;
@synthesize addLabel;
@synthesize countyLabel;
@synthesize myCity;
@synthesize myFormattedAddress;
@synthesize MyCountry;
@synthesize destData;
@synthesize myState;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    NSString * CITY = myCity;
    NSLog(@"%@",CITY);
    
    NSString *stateMy=myState;
    NSLog(@"%@",stateMy);
    
    NSString *myCountry=MyCountry;
    NSLog(@"%@",myCountry);
    
    NSString *formAddress=myFormattedAddress;
    NSLog(@"%@",formAddress);
    
    cityTextField.text=[NSString stringWithFormat:@"%@",CITY];
    stateTextField.text=[NSString stringWithFormat:@"%@",stateMy];
    nameTextField.text=[NSString stringWithFormat:@"%@",myCountry];
    addLabel.text=[NSString stringWithFormat:@"%@",formAddress];
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
    
 /*  NSString *str1=[destinationViewController objectAtIndex:0];
    
    NSLog(@"%@",str1);
    
    nameTextField.text=[NSString stringWithFormat:@"%@",str1];
    
    NSString *str2=[destdata objectAtIndex:1];
    
    NSLog(@"%@",str2);
    
    cityTextField.text=[NSString stringWithFormat:@"%@",str2];
    
    
    
    NSString *str3=[destdata objectAtIndex:2];
    
    NSLog(@"%@",str3);
    
    stateTextField.text=[NSString stringWithFormat:@"%@",str3];
    
    
    
    NSString *str4=[destData objectAtIndex:3];
    
    NSLog(@"%@",str4);
    
    latitudeTextField.text=[NSString stringWithFormat:@"%@",str4];
    
    NSString *str4=[destdata objectAtIndex:3];
    
    NSLog(@"%@",str4);
    
    longitudeTextField.text=[NSString stringWithFormat:@"%@",str4];
    
    
    //    NSString *str5=[destdata objectAtIndex:5];
    
    //    NSLog(@"%@",str5);
    
    //    nameLabelDisplay.text=[NSString stringWithFormat:@"%@",str5];
    
  */
    

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
